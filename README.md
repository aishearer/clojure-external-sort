# Clojure External Sort #

Sort a file by running:

```
#!bash

clojure external-sort.clj <file-name> <chunk-size>
```
The sorted file will be saved to a file called *output*.

Chunk size is optional and the default chunk size is 400,000 lines.