(ns external-sort
  (:require [clojure.java.io :as io]))

(defn min-index [v]
  (loop [index-of-min 0
         min-so-far nil
         i 0]
    (if (>= i (count v))
      index-of-min
      (if (or (pos? (compare min-so-far (nth v i))) (nil? min-so-far))
        (recur i (nth v i) (inc i))
        (recur index-of-min min-so-far (inc i))))))

(defn merge-lists [lists]
  (with-open [wrtr (io/writer "output")]
    (loop [lists lists]
      (let [lists (vec (remove empty? lists))]
        (if (empty? lists)
          nil
          (let [index-of-smallest (min-index (vec (map first lists)))]
            (.write wrtr (first (nth lists index-of-smallest)))
            (.newLine wrtr)
            (recur (update-in lists [index-of-smallest] rest))))))))

(defn split-file [chunk-size data-set]
  (with-open [rdr (io/reader data-set)]
    (doseq [[chunk-num chunk] (map-indexed vector (partition chunk-size (line-seq rdr)))]
      (with-open [wrtr (io/writer (str "tmp/" chunk-num))]
        (doseq [line (sort chunk)]
          (.write wrtr line)
          (.newLine wrtr))))))

(defn file-count []
  (let [directory (io/file "tmp/") files (file-seq directory)]
    (- (count files) 1)))

(defn open-files [file-count]
  (let [file-names (map #(str "tmp/" %) (range file-count))]
    (vec (map #(line-seq (io/reader %)) file-names))))

(def file-name (first *command-line-args*))
(def chunk-size (read-string (or (second *command-line-args*) "400000")))
(if (nil? file-name)
  (println "I need a file name as an argument!")
  (do
    (.mkdir (java.io.File. "tmp"))
    (split-file chunk-size file-name)
    (merge-lists (open-files (file-count)))))
